import asyncio
import logging
from typing import List

import aiohttp
import requests

from blockchein_interface import DemoWalletABC


class WalletInsufficientFunds(Exception):
    def __str__(self):
        return 'Insufficient funds'


class WalletRPCError(Exception):
    def __init__(self, code: int, message: str):
        self.errno = code
        self.message = message

    def __str__(self):
        return f'WalletRPCError (%s: %s)' % (self.errno, self.message)


class CardanoDemoWallet(DemoWalletABC):
    def __init__(self, rest_url, wallet_api_url, wallet_id):
        self.transaction_response: List = []
        self.blocks_response: List = []
        self.rest_url: str = rest_url
        self.wallet_api_url = wallet_api_url
        self.request_headers = {"Content-Type": "application/json"}
        self.passphrase = "Blablabla blablablablabla"
        self.passphrase2 = "Blablabla blablablablabla bloeblin"
        self.wallet_id = wallet_id

    def request(self, url: str, payload=None, method: str = 'get') -> dict:
        if not payload:
            payload = {}
        data = requests.get(url, params=payload)
        if method == 'post':
            data = requests.post(url, json=payload, headers=self.request_headers)
        if data.status_code != 200:
            logging.getLogger('wallet.eth').error('[-] %s', data.json().get('message'))
            raise WalletRPCError(data.status_code, data.json().get('message'))
        return data.json()

    async def requestAsync(self, url: str, method: str = 'get', params=None) -> dict:
        """
        :param url: api path
        :param method: get or post
        :param params: payload for request
        :return: dict
        """
        if params is None:
            params = {}
        async with aiohttp.ClientSession() as session:
            s = session.post if method == 'post' else session.get
            r = s(url, json=params,
                  headers=self.request_headers)
            async with r as response:
                data = await response.json()
                if response.status != 200:
                    logging.getLogger('wallet.eth').error('[-] %s', data['message'])
                    raise WalletRPCError(response.status, f"{data['code']} {data['message']}")
                return data

    def isAddress(self, address) -> bool:
        return True

    def newAddress(self, name=None):
        pass

    def getAddresses(self):
        return self.request(f"{self.wallet_api_url}/wallets/{self.wallet_id}/addresses")

    def toAmount(self, amount):
        pass

    def fromAmount(self, amount):
        pass

    def getHeight(self):
        r = self.request(f"{self.rest_url}/api/blocks/pages/")
        block_height = None
        try:
            block_height = r['Right'][1][0]['cbeBlkHeight']
        except (KeyError, IndexError) as ex:
            print(ex)
        return block_height

    def getTx(self, txid, wallet_id=None):
        tx = self.request(f"{self.rest_url}/api/txs/summary/{txid}")
        fee = int(tx['Right']['ctsFees']['getCoin'])
        io = [
            {'address': [x['ctaAddress'] for x in tx['Right']['ctsInputs']], 'dir': 'send',
             'amount': sum([int(x['ctaAmount']['getCoin']) for x in tx['Right']['ctsInputs']]) + fee, 'account': None,
             'ccy': 'ADA'},
            {'address': [x['ctaAddress'] for x in tx['Right']['ctsOutputs']], 'dir': 'recv',
             'amount': sum([int(x['ctaAmount']['getCoin']) for x in tx['Right']['ctsInputs']]), 'account': None,
             'ccy': 'ADA'},
        ]
        timestamp = tx['Right']['ctsTxTimeIssued']

        confirmations = self.getHeight() - tx['Right']['ctsBlockHeight']

        return {
            'txid': txid,
            'io': io,
            'fee': fee,
            'time': timestamp,
            'confirmations': confirmations
        }

    def getFeePrice(self, payments=None):
        # address = self.getAddresses()[0]['id']
        # https://input-output-hk.github.io/cardano-wallet/api/edge/#operation/postTransactionFee
        r = self.request(f"{self.wallet_api_url}/wallets/{self.wallet_id}/payment-fees",
                         payload={"payments": payments},
                         method='post')
        return int(r["estimated_max"]["quantity"])

    def getBalance(self, address):
        r = self.request(f"{self.wallet_api_url}/wallets/{address}/")
        return r["balance"]["total"]["quantity"]

    def send(self, to, amount=None, includefee=False) -> dict:
        fr = self.getAddresses()[0]['id']
        return self.transfer(fr, to, amount, includefee)

    def transfer(self, fr, to, amount=None, includefee=False):
        balance = self.getBalance(fr)
        # amount = (amount)

        payments = [
            {
                "address": fr,
                "amount": {
                    "quantity": amount,
                    "unit": "lovelace"
                }
            },
        ]
        fee = self.getFeePrice(payments)
        if includefee:
            amount -= fee
        if amount + fee > balance:
            raise WalletInsufficientFunds()

        data = {"passphrase": self.passphrase,
                "payments": [{
                    "address": to,
                    "amount": {
                        "quantity": amount,
                        "unit": "lovelace"
                    }
                }]}
        return self.request(f"{self.wallet_api_url}/wallets/{fr}/transactions", payload=data, method='post')

    def getVersion(self):
        pass

    async def processBlock(self, block):
        logging.getLogger('wallet.cardano').info('New block %s', block)

    async def processTx(self, txid):
        logging.getLogger('wallet.cardano').info('New tx %s', txid)

    async def handler(self):
        logging.getLogger('wallet.cardano').info('Cardano stream start')
        while True:
            async with aiohttp.ClientSession() as session:
                transactions = await self.requestAsync(f'{self.rest_url}/api/txs/last')
                for tr in transactions['Right']:
                    if tr not in self.transaction_response:
                        asyncio.ensure_future(self.processTx(tr['cteId']))
                self.transaction_response = transactions['Right']
                await asyncio.sleep(1)
                blocks = await self.requestAsync(f'{self.rest_url}/api/blocks/pages')
                for block in blocks['Right'][1]:
                    if block not in self.blocks_response:
                        asyncio.ensure_future(self.processBlock(block["cbeBlkHeight"]))
                self.blocks_response = blocks['Right'][1]

    def createWallet(self, name):
        mnemonic_sentence = ["melody", "tree", "phone", "impulse",
                             "column", "grain", "crumble", "slot", "cube",
                             "tunnel", "target", "kite", "nephew", "cover", "else"]

        mnemonic_sentence_factor = ["melody", "tree", "phone", "impulse",
                                    "column", "grain", "crumble", "slot", "cube"]

        data = {"name": name, "mnemonic_sentence": mnemonic_sentence, "style": "random",
                "mnemonic_sentence_factor": mnemonic_sentence_factor,
                "passphrase": self.passphrase, "address_pool_gap": 20}
        return self.request(f"{self.wallet_api_url}/wallets", method='post', payload=data)


if __name__ == '__main__':
    # Logging
    sh = logging.StreamHandler()
    logging.getLogger('wallet').addHandler(sh)
    logging.getLogger('wallet').setLevel(logging.DEBUG)

    wallet = CardanoDemoWallet(rest_url='http://localhost:8100', wallet_api_url='http://localhost:8083/v2',
                               wallet_id="d7d3036e04a8b7b08ee06be0b9c75f6d3cc83c14")
    print('Height', wallet.getHeight())
    try:
        print('Address', wallet.createWallet(name="Alan's Wallet"))
    except (WalletInsufficientFunds, WalletRPCError) as ex:
        print(ex)
    print('Addresses', wallet.getAddresses())
    try:
        print('Estimate Fee', wallet.getFeePrice(wallet.getAddresses()[1]['id']))
    except (WalletInsufficientFunds, WalletRPCError) as ex:
        print(ex)
    try:
        print('Send', wallet.send(
            "addr1qr8n6qnnmxu6d4tyzl7c5skdu8x9s77xqcpn4andeau9rpg2vsnpq3nw4wpewadesuwmxes8w77ndgpd27s7p9kam70sm45jke",
            6,
            includefee=True))
    except (WalletInsufficientFunds, WalletRPCError) as ex:
        print(ex)
    print('Tx', wallet.getTx('af51849443746c6a3f5a58ddd13f210d0d92bf97a2c071faf6bb65089c03ab16'))
    print('Tx', wallet.getTx('c700cc433a0f8b3159fa848f22380a8da75425f62394ac04f363f2d84baedf22'))
    loop = asyncio.get_event_loop()
    loop.run_until_complete(wallet.handler())
