from abc import ABCMeta, abstractmethod


class DemoWalletABC(metaclass=ABCMeta):
    @abstractmethod
    def request(self, *args, **kwargs):
        pass

    @abstractmethod
    async def requestAsync(self, *args, **kwargs):
        pass

    @abstractmethod
    def isAddress(self, *args, **kwargs) -> bool:
        return True

    @abstractmethod
    def newAddress(self, *args, **kwargs):
        pass

    @abstractmethod
    def getAddresses(self):
        pass

    @abstractmethod
    def toAmount(self, *args, **kwargs):
        pass

    @abstractmethod
    def fromAmount(self, *args, **kwargs):
        pass

    @abstractmethod
    def getHeight(self):
        pass

    @abstractmethod
    def getTx(self, *args, **kwargs):
        pass

    @abstractmethod
    def getFeePrice(self):
        pass

    @abstractmethod
    def getBalance(self, *args, **kwargs):
        pass

    @abstractmethod
    def send(self, *args, **kwargs):
        pass

    @abstractmethod
    def transfer(self, *args, **kwargs):
        pass

    @abstractmethod
    def getVersion(self):
        pass

    @abstractmethod
    async def processBlock(self, *args, **kwargs):
        pass

    @abstractmethod
    async def processTx(self, *args, **kwargs):
        pass

    @abstractmethod
    async def handler(self):
        pass
