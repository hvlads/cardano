#!/usr/bin/env bash

set +e

# Creating a user manually
# sudo adduser cardano
# sudo usermod -aG sudo cardano
# sudo su - cardano

# Install dependencies
sudo yum update -y
sudo yum install git gcc gcc-c++ tmux gmp-devel make tar wget zlib-devel libtool autoconf -y
sudo yum install systemd-devel ncurses-devel ncurses-compat-libs -y

# Download, unpack, install and update Cabal
wget https://downloads.haskell.org/~cabal/cabal-install-3.2.0.0/cabal-install-3.2.0.0-x86_64-unknown-linux.tar.xz
tar -xf cabal-install-3.2.0.0-x86_64-unknown-linux.tar.xz
rm cabal-install-3.2.0.0-x86_64-unknown-linux.tar.xz cabal.sig
mkdir -p ~/.local/bin
mv cabal ~/.local/bin/

export PATH="$HOME.local/bin:$PATH"
source .bashrc
cabal update
cabal --version

# Download and install GHC
wget https://downloads.haskell.org/~ghc/8.6.5/ghc-8.6.5-x86_64-deb9-linux.tar.xz
tar -xf ghc-8.6.5-x86_64-deb9-linux.tar.xz
rm ghc-8.6.5-x86_64-deb9-linux.tar.xz
cd ghc-8.6.5 || exit
./configure
sudo make install

# Install Libsodium
git clone https://github.com/input-output-hk/libsodium
cd libsodium || exit
git checkout 66f017f1
./autogen.sh
./configure
make
sudo make install
export LD_LIBRARY_PATH="/usr/local/lib:$LD_LIBRARY_PATH"
export PKG_CONFIG_PATH="/usr/local/lib/pkgconfig:$PKG_CONFIG_PATH"

# Download the source code for cardano-node
git clone https://github.com/input-output-hk/cardano-node.git
cd cardano-node || exit

TAGGED_VERSION='pioneer'
git fetch --all --tags
git tag
git checkout tags/$TAGGED_VERSION

# Build and install the node
cabal build all

cp -p $HOME/cardano/cardano-node/dist-newstyle/build/x86_64-linux/ghc-8.6.5/cardano-node-1.11.0/x/cardano-node/build/cardano-node/cardano-node $HOME/.local/bin/
cp -p $HOME/cardano/ghc-8.6.5/libsodium/cardano-node/dist-newstyle/build/x86_64-linux/ghc-8.6.5/cardano-cli-1.11.0/x/cardano-cli/build/cardano-cli/cardano-cli $HOME/.local/bin/

# Check the version installed:
cardano-cli --version

# Get configuration files

# For Cardano testnet
wget https://hydra.iohk.io/job/Cardano/cardano-node/cardano-deployment/latest-finished/download/1/testnet-config.json
wget https://hydra.iohk.io/job/Cardano/cardano-node/cardano-deployment/latest-finished/download/1/testnet-byron-genesis.json
wget https://hydra.iohk.io/job/Cardano/cardano-node/cardano-deployment/latest-finished/download/1/testnet-shelley-genesis.json
wget https://hydra.iohk.io/job/Cardano/cardano-node/cardano-deployment/latest-finished/download/1/testnet-topology.json

# For Mainnet:

#wget https://hydra.iohk.io/job/Cardano/cardano-node/cardano-deployment/latest-finished/download/1/mainnet-config.json
#wget https://hydra.iohk.io/job/Cardano/cardano-node/cardano-deployment/latest-finished/download/1/mainnet-byron-genesis.json
#wget https://hydra.iohk.io/job/Cardano/cardano-node/cardano-deployment/latest-finished/download/1/mainnet-shelley-genesis.json
#wget https://hydra.iohk.io/job/Cardano/cardano-node/cardano-deployment/latest-finished/download/1/mainnet-topology.json

#cardano-node run --cabal
PUBLIC_IP=91.201.40.182
mkdir db
cardano-node run \
   --topology $PWD/testnet-topology.json \
   --database-path db \
   --socket-path /db/node.socket \
   --host-addr $PUBLIC_IP \
   --port 3001 \
   --config $PWD/testnet-config.json

export CARDANO_NODE_SOCKET_PATH=/db/node.socket