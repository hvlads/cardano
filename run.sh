#!/usr/bin/env bash

set +e

cd cardano-rest || exit
NETWORK=testnet docker-compose  -f docker-compose-all.yaml up